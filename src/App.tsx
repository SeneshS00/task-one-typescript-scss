import React from "react";
import "./App.scss";
import LayoutComponent from "./Components/Pages/layoutComponent";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

class App extends React.Component {
  render() {
    return (
      <div className="App">
        <Router>
          <Switch>
            <Route path="/nav1">
              <LayoutComponent content="ffff"></LayoutComponent>
            </Route>
            <Route path="/nav2">
              <LayoutComponent content="navTwo"></LayoutComponent>
            </Route>
            <Route path="/nav3">
              <LayoutComponent content="navThree"></LayoutComponent>
            </Route>
          </Switch>
        </Router>
      </div>
    );
  }
}

export default App;
