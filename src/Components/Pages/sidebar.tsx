import React, { Component, FC } from 'react';
import { Layout, Menu } from 'antd';
import './sidebar.scss'
import {
    UserOutlined,
    VideoCameraOutlined,
    UploadOutlined,
  } from '@ant-design/icons';

const {Sider} = Layout;
let key: string;

type IsCollapsed = {
  collapsed: boolean;
}


//class Sidebar extends Component {
const Sidebar: FC<IsCollapsed> = (props) =>   {
  
      return(
        <div >
            <div className='sidebardiv1'> 
            <Sider trigger={null} collapsible collapsed={props.collapsed} style={{ height:'100vh' }}>
            <div className="logo">
            logo
            </div>
            <Menu theme="dark" mode="inline" defaultSelectedKeys={[window.location.pathname]}>
                <Menu.Item key="/nav1" icon={<UserOutlined />} >
                    <a href="/nav1" ><span>nav1</span></a>
                </Menu.Item>
                <Menu.Item key="/nav2" icon={<VideoCameraOutlined />}>
                    <a href="/nav2"><span>nav2</span></a>
                </Menu.Item>
                <Menu.Item key="/nav3" icon={<UploadOutlined />}>
                    <a href="/nav3"><span>nav3</span></a>
                </Menu.Item> 
            </Menu>
            </Sider>
            
            </div>   
        </div>
      
      );
    
  
    };

export default Sidebar;