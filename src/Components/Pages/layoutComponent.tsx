import React, { FC, useEffect, useState } from "react";
import { Layout } from "antd";
import {
  MenuUnfoldOutlined,
  MenuFoldOutlined,
  UserOutlined,
} from "@ant-design/icons";
import { Avatar } from "antd";
import FooterConponent from "./footerConponent";
import Sidebar from "./sidebar";
import ContentComp from "./contentComp";

const { Header, Footer } = Layout;

type LayoutProps = {
  content: string;
};


const LayoutComponent: FC<LayoutProps> = (props) => {

  const [collapsed, setCollapsed] = useState(false);
  
  console.log("1:"+collapsed);

  
  localStorage.setItem('CollapsedLS', 'T');

   const collapsedLSGot = localStorage.getItem('CollapsedLS');
//   console.log(cat);

  //  if(collapsedLSGot === "T"){
  //    setCollapsed(true);
  //  }
  console.log("2:"+collapsed);

  function toggle() {
    setCollapsed(!collapsed);
    console.log("3:"+collapsed);
  }
  

  return (
    <div className="App">
      <Layout className="main-layout">
        <Sidebar collapsed={collapsed}/>
        <Layout className="site-layout">
          <Header
            className="site-layout-background"
            style={{
              padding: 0,
              textAlign: "right",
              border: "1px solid red",
            }}
          >
             
             {/* {React.createElement(
              collapsed ? MenuUnfoldOutlined : MenuFoldOutlined,
              {
                className: "trigger",
                onClick: toggle,
              }
            )} */}

            {collapsed
                    ? <MenuUnfoldOutlined className={"trigger"} onClick={toggle} ></MenuUnfoldOutlined>
                    : <MenuFoldOutlined className={"trigger"} onClick={toggle} ></MenuFoldOutlined>
                  }

            <Avatar
              style={{ backgroundColor: "#87d068", margin: "10px" }}
              icon={<UserOutlined />}
            />
          </Header>
          <ContentComp>{props.content}</ContentComp>
          <Footer style={{ textAlign: "center", background: "#FFF" }}>
            {" "}
            <FooterConponent />{" "}
          </Footer>
        </Layout>
      </Layout>
    </div>
  );
};


export default LayoutComponent;





