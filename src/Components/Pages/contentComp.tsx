import React, { Component, FC } from "react";
import { Layout, Menu } from "antd";

const { Content } = Layout;

const ContentComp: FC = (props) => {
  return (
    <Content
      className="site-layout-background"
      style={{
        margin: "24px 16px",
        padding: 24,
        minHeight: 280,
      }}
    >
      {props.children}
    </Content>
  );
};

export default ContentComp;
